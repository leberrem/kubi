
resource "aws_instance" "output_host" {
  ami                    = "${data.aws_ami.centos.image_id}"
  instance_type          = "${var.output_instance_type}"
  key_name               = "${aws_key_pair.key.key_name}"
  vpc_security_group_ids = ["${aws_security_group.output_host.id}"]
  subnet_id              = "${data.terraform_remote_state.network.private_subnets_id_list[2]}"

  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
    volume_size           = "10"
  }

  tags {
    Name        = "${local.init_tags["name"]}-output-0"
    Environment = "${local.init_tags["env"]}"
    Owner       = "${local.init_tags["owner"]}"
    Stack       = "${local.init_tags["stack"]}"
  }
}


resource "aws_security_group" "output_host" {
  vpc_id = "${data.terraform_remote_state.network.vpc_id}"

  name        = "${local.init_tags["name"]}-k8s-ouput"
  description = "Applied to k8s-ouput"

  tags {
    Environment = "${local.init_tags["env"]}"
    Owner       = "${local.init_tags["owner"]}"
    Stack       = "${local.init_tags["stack"]}"
  }
}

resource "aws_security_group_rule" "output_host_in" {
  security_group_id = "${aws_security_group.output_host.id}"

  type      = "ingress"
  protocol  = "all"
  from_port = 0
  to_port   = 0

  cidr_blocks = ["${data.terraform_remote_state.network.vpc_cidr}"]
}

resource "aws_security_group_rule" "output_host_out" {
  security_group_id = "${aws_security_group.output_host.id}"

  type      = "egress"
  protocol  = "all"
  from_port = 0
  to_port   = 0

  cidr_blocks = ["0.0.0.0/0"]
}
