output "az_list" {
  value = ["a", "b", "c"]
}

output "vpc_id" {
  value = "${module.vpc_layout.vpc_id}"
}

output "vpc_cidr" {
  value = "${module.vpc_layout.vpc_cidr}"
}

# ------------------------------------------------------------------------------

output "public_subnet_id_a" {
  value = "${module.vpc_layout.public_subnet_id_a}"
}

output "public_subnet_id_b" {
  value = "${module.vpc_layout.public_subnet_id_b}"
}

output "public_subnet_id_c" {
  value = "${module.vpc_layout.public_subnet_id_c}"
}

# ------------------------------------------------------------------------------

output "public_subnet_cidr_a" {
  value = "${module.vpc_layout.public_subnet_cidr_a}"
}

output "public_subnet_cidr_b" {
  value = "${module.vpc_layout.public_subnet_cidr_b}"
}

output "public_subnet_cidr_c" {
  value = "${module.vpc_layout.public_subnet_cidr_c}"
}

# ------------------------------------------------------------------------------

output "private_subnet_id_a" {
  value = "${module.vpc_layout.private_subnet_id_a}"
}

output "private_subnet_id_b" {
  value = "${module.vpc_layout.private_subnet_id_b}"
}

output "private_subnet_id_c" {
  value = "${module.vpc_layout.private_subnet_id_c}"
}

# ------------------------------------------------------------------------------
output "private_subnets_id_list" {
  value = [
    "${module.vpc_layout.private_subnet_id_a}",
    "${module.vpc_layout.private_subnet_id_b}",
    "${module.vpc_layout.private_subnet_id_c}",
  ]
}

output "private_subnet_cidr_a" {
  value = "${module.vpc_layout.private_subnet_cidr_a}"
}

output "private_subnet_cidr_b" {
  value = "${module.vpc_layout.private_subnet_cidr_b}"
}

output "private_subnet_cidr_c" {
  value = "${module.vpc_layout.private_subnet_cidr_c}"
}

output "init_tags" {
    value = {
    basename  = "proto"
    name  = "proto"
    env   = "demo"
    owner = "proto"
    stack = "kubi"
  }
}